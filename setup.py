import os
import numpy as np
from setuptools import Extension, dist, find_packages, setup
from Cython.Build import cythonize  # noqa: E402, isort:skip

def make_cython_ext(name, module, sources):
    extension = Extension(
        '{}.{}'.format(module, name),
        [os.path.join(*module.split('.'), p) for p in sources],
        include_dirs=[np.get_include()],
        language='c++')
    extension, = cythonize(extension)
    return extension

def get_requirements(filename='requirements.txt'):
    here = os.path.dirname(os.path.realpath(__file__))
    with open(os.path.join(here, filename), 'r') as f:
        requires = [line.replace('\n', '') for line in f.readlines()]
    return requires

if __name__ == '__main__':

    setup(
        name='hsi-cnn',
        description='Regression Model Based on CNN for Hypersepctral Image',
        author='Haihua, Mao',
        author_email='haihuam@outlook.com',
        license='Apache License 2.0',
        setup_requires=['cython', 'numpy'],
        packages=find_packages(),
        install_requires=get_requirements(),
        ext_modules = [
            make_cython_ext(
                name='generate_geo_index',
                module='utils',
                sources=['src/generate_geo_index.pyx']),
            ],
        )
