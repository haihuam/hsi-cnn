# HSI-CNN

## Install
```shell
# Use Anaconda create a virtual env
conda create -n hsi-cnn python=3.7.6
conda activate hsi-cnn
# Install requirements.
pip install -r requirements.txt
# Build project
python setup.py build_ext --inplace
```

## Build dataset
```shell
python build_data.py --patch-size [patch-size] [list: dates-list]
```

## Project structure
> This project contains a `Data` repo and `Src` repo, and `Data` is composed by `coords_data`, `depth_data`, `gf5_data`.
>   `coords_data` is dumped from ENVI
>   `depth_data` is the ground truth with depth label
>   `gf5_data` is hyspectral image data

```shell
|-- Data
|   |-- coords_data
|   |   |-- GF5_AHSI_E121.55_N31.81_20190524_005540_L10000045392_SW.coords.txt
|   |   |-- GF5_AHSI_E121.68_N31.31_20190524_005540_L10000045388_SW.coords.txt
|   |   |-- GF5_AHSI_E121.81_N30.82_20190524_005540_L10000045391_SW.coords.txt
|   |   |-- GF5_AHSI_E121.91_N31.81_20190327_004698_L10000038987_SW.coords.txt
|   |   |-- GF5_AHSI_E122.02_N31.31_20190204_003955_L10000032641_SW.coords.txt
|   |   `-- GF5_AHSI_E122.04_N31.31_20190327_004698_L10000038978_SW.coords.txt
|   |-- depth_data
|   |   |-- 2018_07_08.txt
|   |   |-- 2018_07_15.txt
|   |   |-- 2018_09_04.txt
|   |   `-- 2019_03_27.txt
|   |-- gf5_data
|   |   |-- 2019-0204
|   |   |-- 2019-0327
|   |   `-- 2019-0524
|   `-- train
|       |-- train.npz
|       |-- train_patch_2.npz
|       `-- val_patch_2.npz
`-- Src
    |-- build_data.py
    |-- configs
    |   |-- data.cfg
    |   |-- flow_gen_data.py
    |   |-- __init__.py
    |   `-- __pycache__
    |-- docs
    |   |-- build
    |   |-- make.bat
    |   |-- Makefile
    |   `-- source
    |-- models
    |-- README.md
    |-- requirements.txt
    |-- setup.py
    `-- utils
        |-- extract_features.py
        |-- func_utils.py
        |-- gdal_utils.py
        |-- generate_geo_index.cpython-37m-x86_64-linux-gnu.so
        |-- __init__.py
        |-- __pycache__
        `-- src
```

## data.cfg
This is the config file to guide the tool how to build training data.

```json
{
"depth_data": {
	       "2018_07_08":{
		       "depth_txt": "depth_data/2018_07_08.txt"
	       },
	       "2018_07_15":{
		       "depth_txt": "depth_data/2018_07_15.txt"
	       },
	       "2018_09_04":{
		       "depth_txt": "depth_data/2018_09_04.txt"
	       },
	       "2019_03_27":{
		       "depth_txt": "depth_data/2019_03_27.txt",
		       "geo_datas":[
			            "20190327_004698_L10000038987_SW", 
			            "20190327_004698_L10000038978_SW",
				    "20190204_003955_L10000032641_SW"
		                   ]
	       }
              },
"geo_data": {
	     "20190327_004698_L10000038978_SW":{
		     "geotiff": "gf5_data/2019-0327/GF5_AHSI_E122.04_N31.31_20190327_004698_L10000038978/GF5_AHSI_E122.04_N31.31_20190327_004698_L10000038978_SW.geotiff",
		     "coords_txt": "coords_data/GF5_AHSI_E122.04_N31.31_20190327_004698_L10000038978_SW.coords.txt",
		     "proportion": 0.9,
		     "mask": []
		              
	     },
	     "20190327_004698_L10000038987_SW":{
		     "geotiff": "gf5_data/2019-0327/GF5_AHSI_E121.91_N31.81_20190327_004698_L10000038987/GF5_AHSI_E121.91_N31.81_20190327_004698_L10000038987_SW.geotiff",
		     "coords_txt": "coords_data/GF5_AHSI_E121.91_N31.81_20190327_004698_L10000038987_SW.coords.txt",
		     "proportion": 0.9,
		     "mask": []
	     },
	     "20190204_003955_L10000032641_SW":{
		     "geotiff": "gf5_data/2019-0204/GF5_AHSI_E122.02_N31.31_20190204_003955_L10000032641/GF5_AHSI_E122.02_N31.31_20190204_003955_L10000032641_SW.geotiff",
		     "coords_txt": "coords_data/GF5_AHSI_E122.02_N31.31_20190204_003955_L10000032641_SW.coords.txt",
		     "proportion": 0.9,
		     "mask": [[0, 500, 900, 1000], 
		              [1000, 1500, 2000, 2080]]
	     },
	     "20190524_005540_L10000045392_SW": {
		     "geotiff": "gf5_data/2019-0524/GF5_AHSI_E121.55_N31.81_20190524_005540_L10000045392/GF5_AHSI_E121.55_N31.81_20190524_005540_L10000045392_SW.geotiff",
		     "coords_txt": "coords_data/GF5_AHSI_E121.55_N31.81_20190524_005540_L10000045392_SW.coords.txt",
		     "proportion": 0.9,
		     "mask": []
	     },
             "20190524_005540_L10000045388_SW": {
		     "geotiff": "gf5_data/2019-0524/GF5_AHSI_E121.68_N31.31_20190524_005540_L10000045388/GF5_AHSI_E121.68_N31.31_20190524_005540_L10000045388_SW.geotiff",
		     "coords_txt": "coords_data/GF5_AHSI_E121.68_N31.31_20190524_005540_L10000045388_SW.coords.txt",
		     "proportion": 0.9,
		     "mask": []
	     },
             "20190524_005540_L10000045391_SW": {
		     "geotiff": "gf5_data/2019-0524/GF5_AHSI_E121.81_N30.82_20190524_005540_L10000045391/GF5_AHSI_E121.81_N30.82_20190524_005540_L10000045391_SW.geotiff",
		     "coords_txt": "coords_data/GF5_AHSI_E121.81_N30.82_20190524_005540_L10000045391_SW.coords.txt",
		     "proportion": 0.9,
		     "mask": []
	     }
            }
}

```

