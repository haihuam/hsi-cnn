from osgeo import gdal

def gdal_reader(geotiff_file):
    """
    Function: common geotiff reader
    parameters: tiff file
    returns: xsize, ysize, raster_count, data_loader function
    """
    dataset = gdal.Open(geotiff_file, gdal.GA_ReadOnly)
    xsize = dataset.RasterXSize
    ysize = dataset.RasterYSize
    raster_count = dataset.RasterCount
    return xsize, ysize, raster_count, dataset.GetVirtualMemArray
